# docker-junit-extension

## Table of content

* [What is it?](#what-is-it)
* [How to build](#how-to-build)
* [Maven Profiles](#maven-profiles)
    * [Integration tests](#integration-tests)
* [How to use it](#how-to-use-it)

## What is it?

A JUnit extension that allow start properly configured containers and use them in integration tests.

## How to use it

Annotate you test class with `DockerExtension` and declare the containers configuration you want to set up on your test:

```
@DockerExtension({
    @DockerContainer(
        image = "postgres:11",
        alias = "postgres",
        ports = { @Port(internal = 5432) },
        mounts = { @Mount(value = "/init.sql") },
        waitFor = @WaitFor("[1] LOG:  database system is ready to accept connections"),
        whenReuse = WhenReuse.ALWAYS)
})
public class DockerExtensionIt {

  @Test
  public void test(@ContainerParam("postgres") Container postgres) throws Exception {
    postgres.exec("psql", "-U", "postgres", "-f", "/init.sql")
      .forEach(line -> System.out.println(line));
    try (Connection connection = DriverManager.getConnection(
        "jdbc:postgresql://localhost:" + postgres.getPort(5432) + "/postgres?user=postgres");
        Statement s = connection.createStatement();
        ResultSet rs = s.executeQuery("SELECT COUNT(*) > 0 FROM test")) {
      Assertions.assertTrue(rs.next());
      Assertions.assertFalse(rs.getBoolean(1));
    }
  }

}
```

## How to build

Java 8 JDK and Maven are required to build this project.

Run following command:

```
mvn clean package
```

## Maven Profiles

- Safer: Slower but safer profile used to look for errors before pushing to SCM 

```
mvn verify -P safer
```

### Integration tests

The integration test suite requires that Docker is installed on the system and available to the user. 
To launch the integrations tests run the following command:

```
mvn verify -P integration
```

To run integration tests with Java debugging enabled on port 8000:

```
mvn verify -P integration -Dmaven.failsafe.debug="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000"
```
