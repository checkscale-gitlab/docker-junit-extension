/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

/**
 * Describes an environment variable to set for a docker container.
 *
 * @since 1.0.0
 */
public @interface Environment {

  /**
   * the environment's variable name.
   */
  String key();

  /**
   * the environment's variable value
   * (${VAR} will expand to the environment variable VAR,
   * use '\' to protect '$' and '\').
   */
  String value();
}
