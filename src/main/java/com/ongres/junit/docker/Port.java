/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

/**
 * Describes a port to expose for a docker container.
 *
 * <p>
 * Bind an internal container port to an external (local to the host) port.
 * If an external port is not specified docker will pick one for you among availables ports.
 * You can specify port type TCP or UDP, by default will be TCP.
 * </p>
 *
 * @since 1.0.0
 */
public @interface Port {

  /**
   * Internal port.
   */
  int internal();

  /**
   * External port.
   */
  int external() default -1;

  /**
   * Network protocol.
   */
  NetworkProtocol protocol() default NetworkProtocol.TCP;
}
