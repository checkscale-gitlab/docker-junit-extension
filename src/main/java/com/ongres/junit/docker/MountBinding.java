/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Objects;
import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.ongres.junit.docker.Mount.DefaultReference;

@JsonDeserialize(builder = MountBinding.Builder.class)
public class MountBinding {

  public final String reference;
  public final String resource;
  public final String temp;
  public final String path;
  public final boolean system;

  private MountBinding(Builder builder) {
    this(builder.reference, builder.resource, builder.temp, builder.path, builder.system);
  }

  protected MountBinding(String reference, String resource, String temp, String path,
      boolean system) {
    Preconditions.checkNotNull(reference);
    Preconditions.checkNotNull(resource);
    Preconditions.checkNotNull(temp);
    Preconditions.checkNotNull(path);

    this.reference = reference;
    this.resource = resource;
    this.temp = temp;
    this.path = path;
    this.system = system;
  }

  public String getReference() {
    return reference;
  }

  public String getResource() {
    return resource;
  }

  public String getTemp() {
    return temp;
  }

  public String getPath() {
    return path;
  }

  public boolean isSystem() {
    return system;
  }

  @JsonIgnore
  public String getSystemPath() {
    return system ? resource : temp;
  }

  @JsonIgnore
  public String getDockerIdentifier() {
    return system ? resource + ":" + path : temp + ":" + path;
  }

  /**
   * Copy resource to temp folder (creating it if not exists).
   */
  void copyToTemp() {
    try {
      Class<?> reference = Class.forName(this.reference);
      boolean useResourceFolder = resource.startsWith("@");
      URI resourceUri;
      if (useResourceFolder) {
        resourceUri = reference.getResource(resource.substring(1)).toURI();
      } else {
        resourceUri = reference.getResource(resource).toURI();
      }

      try (Closeable closeable = createFileSystemIfNotFound(resourceUri)) {
        Path resourcePath = useResourceFolder
            ? Paths.get(resourceUri).getParent() : Paths.get(resourceUri);
        Path sourcePath = Files.isDirectory(resourcePath) ? resourcePath : resourcePath.getParent();
        Path targetPath = Paths.get(temp);
        Files.createDirectories(targetPath);
        Files.walkFileTree(resourcePath, new DeleteVisitor(sourcePath, targetPath));
        Files.walkFileTree(resourcePath, new CopyVisitor(sourcePath, targetPath));
      }
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  void deleteTemp() {
    try {
      Path targetPath = Paths.get(temp);
      if (!Files.exists(targetPath)) {
        return;
      }
      Files.walkFileTree(targetPath, new DeleteVisitor(targetPath, targetPath));
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  boolean existsTemp() {
    try {
      Path targetPath = Paths.get(temp);
      return Files.exists(targetPath);
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  private Closeable createFileSystemIfNotFound(URI uri) throws IOException, URISyntaxException {
    try {
      Paths.get(uri);
    } catch (FileSystemNotFoundException ex) {
      return FileSystems.newFileSystem(uri, new HashMap<>());
    }
    return () -> { };
  }

  private class DeleteVisitor extends SimpleFileVisitor<Path> {
    private final Path sourcePath;
    private final Path targetPath;

    private DeleteVisitor(Path sourcePath, Path targetPath) {
      this.sourcePath = sourcePath;
      this.targetPath = targetPath;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc)
        throws IOException {
      Files.deleteIfExists(targetPath.resolve(sourcePath.relativize(dir).toString()));
      return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
        throws IOException {
      Files.deleteIfExists(targetPath.resolve(sourcePath.relativize(file).toString()));
      return FileVisitResult.CONTINUE;
    }
  }

  private class CopyVisitor extends SimpleFileVisitor<Path> {
    private final Path sourcePath;
    private final Path targetPath;

    private CopyVisitor(Path sourcePath, Path targetPath) {
      this.sourcePath = sourcePath;
      this.targetPath = targetPath;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
        throws IOException {
      Files.createDirectories(targetPath.resolve(sourcePath.relativize(dir).toString()));
      return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
        throws IOException {
      Files.copy(file, targetPath.resolve(sourcePath.relativize(file).toString()));
      return FileVisitResult.CONTINUE;
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(path, reference, resource, system);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof MountBinding)) {
      return false;
    }
    MountBinding other = (MountBinding) obj;
    return Objects.equals(path, other.path)
        && Objects.equals(reference, other.reference)
        && Objects.equals(resource, other.resource)
        && system == other.system;
  }

  /**
   * Creates builder to build {@link MountBinding}.
   * @return created builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Builder to build {@link MountBinding}.
   */
  public static final class Builder {
    private String reference = DefaultReference.class.getName();
    private String resource;
    private String temp;
    private String path;
    private boolean system;

    private Builder() {
    }

    public Builder withReference(String reference) {
      this.reference = reference;
      return this;
    }

    public Builder withResource(String resource) {
      this.resource = resource;
      return this;
    }

    public Builder withTemp(String temp) {
      this.temp = temp;
      return this;
    }

    public Builder withPath(String path) {
      this.path = path;
      return this;
    }

    public Builder withSystem(boolean system) {
      this.system = system;
      return this;
    }

    /**
     * Build.
     */
    public MountBinding build() {
      if (temp == null && resource != null) {
        temp = System.getProperty("java.io.tmpdir") + "/docker-junit-extension-mount-"
            + Long.toUnsignedString(new Random().nextLong()) + ".tmp";
      }
      return new MountBinding(this);
    }
  }
}
