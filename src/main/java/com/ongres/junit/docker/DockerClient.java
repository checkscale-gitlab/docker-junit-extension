/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dockerjava.api.exception.DockerException;
import com.ongres.process.ExitCode;
import com.ongres.process.FluentProcess;
import com.ongres.process.Output;
import org.jooq.lambda.Blocking;
import org.jooq.lambda.Seq;
import org.jooq.lambda.Unchecked;
import org.jooq.lambda.tuple.Tuple;
import org.jooq.lambda.tuple.Tuple2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class DockerClient extends AbstractDockerCompatibleClient implements DockerCompatibleClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(DockerClient.class);
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  public DockerClient(String imageRegistryConfigsPath) {
    super(imageRegistryConfigsPath);
  }

  @Override
  public String startContainer(Optional<String> name, String image, List<String> arguments,
      Map<String, String> environment, Collection<PortBinding> ports,
      Collection<MountBinding> mounts, Map<String, String> labels)
      throws DockerException, InterruptedException, IOException {
    String containerId = createContainer(
        name, image, arguments, environment, ports, mounts, labels);
    FluentProcess.start("docker", "start", containerId).join();
    return containerId;
  }

  private String createContainer(
      Optional<String> name, String image, List<String> arguments,
      Map<String, String> environment, Collection<PortBinding> ports,
      Collection<MountBinding> mounts, Map<String, String> labels)
      throws DockerException, InterruptedException, IOException {
    Seq<String> createArgs =
        getCreateContainerArgs(name, image, arguments, environment, ports, mounts, labels);
    return FluentProcess.start("docker", createArgs.prepend("create").toArray(String[]::new))
        .get();
  }

  protected Seq<String> getCreateContainerArgs(Optional<String> name, String image,
      List<String> arguments, Map<String, String> environment, Collection<PortBinding> ports,
      Collection<MountBinding> mounts, Map<String, String> labels) throws IOException {
    final String imageToPull = getImageToPull(image);
    if (!FluentProcess.start("docker", "inspect", image).isSuccessful()) {
      FluentProcess.start("docker", "pull", imageToPull).join();
      if (!imageToPull.equals(image)) {
        FluentProcess.start("docker", "tag", imageToPull, image).join();
      }
    }
    Seq<String> createArgs = Seq.<String>of()
        .append(Seq.seq(labels)
            .flatMap(label -> Seq.of("-l", label.v1 + "=" + label.v2)))
        .append(Seq.seq(createEnvironmentList(environment))
            .flatMap(env -> Seq.of("-e", env)))
        .append(Seq.seq(ports)
            .flatMap(port -> Seq.of("-p",
                port.getExternalPort().map(String::valueOf).map(p -> p + ":").orElse("")
                + port.getInternalPort() + "/" + port.getNetworkProtocol().name())))
        .append(Seq.seq(mounts)
            .peek(mount -> {
              if (!Files.exists(Paths.get(mount.getSystemPath()))) {
                LOGGER.warn("Skipping mount of {} since does not exists",
                    mount.getDockerIdentifier());
              }
            })
            .filter(mount -> Files.exists(Paths.get(mount.getSystemPath())))
            .flatMap(mount -> Seq.of("-v", mount.getDockerIdentifier())))
        .append(name
            .map(n -> Seq.of("--name", n))
            .orElse(Seq.of()))
        .append(image)
        .append(arguments);
    return createArgs;
  }

  @Override
  public void renameContainer(String id, Optional<String> alias)
      throws DockerException, InterruptedException, IOException {
    if (alias.isPresent()) {
      if (!getContainerAlias(id).equals(alias.get())) {
        FluentProcess.start("docker", "rename", id, alias.get()).join();
      }
    } else {
      if (!FluentProcess.start("docker", "inspect", HELLO_WORLD_IMAGE).isSuccessful()) {
        FluentProcess.start("docker", "pull", HELLO_WORLD_IMAGE).join();
      }
      String tmpId = FluentProcess.start("docker", "create", HELLO_WORLD_IMAGE)
          .get();
      String tmpAlias;
      try {
        tmpAlias = getContainerAlias(tmpId);
      } finally {
        FluentProcess.start("docker", "rm", "-fv", tmpId).join();
      }
      FluentProcess.start("docker", "rename", id, tmpAlias).join();
    }
  }

  @Override
  public Optional<String> getContainerId(String alias)
      throws DockerException, InterruptedException, IOException {
    return FluentProcess.start("docker", "ps", "-a", "--format", "{{ .ID }} {{ .Names }}").stream()
        .map(container -> container.split(" "))
        .filter(container -> container[1].equals(alias))
        .map(container -> container[0])
        .findAny();
  }

  @Override
  public Map<String, String> getContainerLabelValues(String label)
      throws DockerException, InterruptedException, IOException {
    return Seq.seq(FluentProcess.start("docker", "ps", "-a", "--format", "{{ .ID }}").stream())
        .map(Unchecked.function(containerId -> FluentProcess.builder(
            "docker", "inspect", containerId)
            .dontCloseAfterLast()
            .start()
            .tryGet()))
        .map(Output::output)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .map(Unchecked.function(container -> OBJECT_MAPPER.readTree(container)))
        .filter(container -> container.has("Labels")
            && container.get("Labels").has(label))
        .map(container -> Tuple.tuple(
            container.get("Id").asText(),
            container.get("Labels").get(label).asText()))
        .toMap(Tuple2::v1, Tuple2::v2);
  }

  @Override
  public String getContainerAlias(String id)
      throws DockerException, InterruptedException, IOException {
    return Seq.seq(OBJECT_MAPPER.readTree(
        FluentProcess.start("docker", "inspect", id).get())
        .elements())
        .map(container -> container.get("Name").asText().substring(1))
        .findAny()
        .orElseThrow(() -> new RuntimeException("Container with id " + id + " not found"));
  }

  @Override
  public boolean isContainerRunning(String id)
      throws DockerException, InterruptedException, IOException {
    return Seq.seq(OBJECT_MAPPER.readTree(
        FluentProcess.start("docker", "inspect", id).get())
        .elements())
        .map(container -> container.get("State").get("Status").asText().equals("running"))
        .findAny()
        .orElseThrow(() -> new RuntimeException("Container with id " + id + " not found"));
  }

  @Override
  public void stopAndRemoveContainerIfExists(String alias)
      throws DockerException, InterruptedException, IOException {
    getContainerId(alias)
        .ifPresent(Unchecked.consumer(containerId -> {
          FluentProcess.start("docker", "rm", "-fv", containerId).join();
        }));
  }

  @Override
  public void stopAndRemoveContainer(String containerId)
      throws DockerException, InterruptedException, IOException {
    FluentProcess.start("docker", "rm", "-fv", containerId).join();
  }

  @Override
  public boolean waitForLog(String[] expectedLines, String containerId, Duration timeout)
      throws InterruptedException, DockerException, ExecutionException, IOException {
    try {
      String containerAlias = getContainerAlias(containerId);
      Stream<String> logStream = FluentProcess.builder("docker", "logs", "-f", containerId)
          .allowedExitCode(ExitCode.SIGKILL)
          .start().stream();
      List<String> expectedLineList = Seq.of(expectedLines).toList();
      return CompletableFuture.supplyAsync(Blocking.supplier(Unchecked.supplier(
          () -> logStream
            .peek(line -> {
              LOGGER.trace("[{}:{}] {}",
                  containerAlias, containerId.substring(0, 10), line);
              if (line.contains(expectedLineList.get(0))) {
                expectedLineList.remove(0);
              }
            })
            .filter(line -> expectedLineList.isEmpty())
            .findAny().isPresent())))
          .thenApply(result -> {
            Unchecked.runnable(logStream::close).run();
            return result;
          })
          .get(timeout.toMillis(), TimeUnit.MILLISECONDS);
    } catch (TimeoutException ex) {
      return false;
    }
  }

  @Override
  public String getContainerIp(String containerId)
      throws DockerException, InterruptedException, IOException {
    return Seq.seq(OBJECT_MAPPER.readTree(
        FluentProcess.start("docker", "inspect", containerId).get())
        .elements())
        .filter(container -> !container.get("NetworkSettings").get("IPAddress").asText().isEmpty())
        .filter(container -> container.has("NetworkSettings")
            && container.get("NetworkSettings").has("IPAddress")
            && !container.get("NetworkSettings").get("IPAddress").asText().isEmpty())
        .map(container -> container.get("NetworkSettings").get("IPAddress").asText())
        .findAny()
        .orElseThrow(() -> new RuntimeException("Container with id " + containerId
            + " not found or IP not set"));
  }

  @Override
  public Integer getContainerBindedPort(String containerId, Integer port, NetworkProtocol protocol)
      throws DockerException, InterruptedException, IOException {
    return Seq.seq(OBJECT_MAPPER.readTree(
        FluentProcess.start("docker", "inspect", containerId).get())
        .elements())
        .filter(container -> !container.get("NetworkSettings").get("IPAddress").asText().isEmpty())
        .flatMap(container -> Seq.seq(container.get("NetworkSettings").get("Ports").fields()))
        .map(containerPort -> Tuple.tuple(containerPort.getKey())
            .map1(internalPort -> internalPort.split("/"))
            .map(internalPortParts -> Tuple.tuple(internalPortParts[0], internalPortParts[1]))
            .concat(containerPort.getValue()))
        .filter(containerPort -> containerPort.v1().equals(String.valueOf(port))
            && containerPort.v2().equals(protocol.getName()))
        .map(containerPort -> containerPort.v3())
        .filter(Objects::nonNull)
        .flatMap(publishedPorts -> Seq.seq(publishedPorts.elements()))
        .map(publishedPort -> publishedPort.get("HostPort").asText())
        .map(Integer::valueOf)
        .findAny()
        .orElseThrow(() -> new RuntimeException("Container with id " + containerId
            + " not found or IP not set"));
  }

  @Override
  public Stream<String> execute(String containerId, String... args)
      throws DockerException, InterruptedException, IOException {
    Stream<String> logStream = FluentProcess.start(
        "docker", Seq.of("exec", containerId).append(args).toArray(String[]::new)).stream();
    return Seq.seq(logStream)
        .onClose(Unchecked.runnable(logStream::close));
  }

  @Override
  public Stream<String> runContainer(Optional<String> alias, String image, List<String> arguments,
      Map<String, String> environment, Set<PortBinding> ports, Collection<MountBinding> mounts,
      Map<String, String> labels) throws DockerException, InterruptedException, IOException {
    Seq<String> createArgs =
        getCreateContainerArgs(alias, image, arguments, environment, ports, mounts, labels);
    Stream<String> logStream = FluentProcess.start(
        "docker", createArgs.prepend("run").toArray(String[]::new)).stream();
    return Seq.seq(logStream).onClose(Unchecked.runnable(logStream::close));
  }

  @Override
  public void copyToContainer(String containerId, Path path, String internalPath)
      throws DockerException, InterruptedException, IOException {
    FluentProcess.start("docker", "cp",
        path.toString(), containerId + ":" + internalPath).join();
  }

  @Override
  public void copyToContainer(String containerId, InputStream inputStream, String internalPath)
      throws DockerException, InterruptedException, IOException {
    FluentProcess.builder("docker", "cp",
        "-", containerId + ":" + internalPath)
        .start()
        .writeToStdin(inputStream);
  }

  @Override
  public InputStream copyFromContainer(String containerId, String internalPath)
      throws DockerException, InterruptedException, IOException {
    return FluentProcess.start("docker", "exec", containerId,
        "tar", "cP", internalPath)
        .withAllowedExitCode(ExitCode.SIGKILL)
        .asInputStream();
  }

}
