/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.github.dockerjava.api.exception.DockerException;
import com.github.dockerjava.api.exception.NotFoundException;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.jooq.lambda.Blocking;
import org.jooq.lambda.Seq;
import org.jooq.lambda.Unchecked;
import org.jooq.lambda.tuple.Tuple;
import org.jooq.lambda.tuple.Tuple2;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressFBWarnings("RV_RETURN_VALUE_IGNORED_BAD_PRACTICE")
final class Extension implements BeforeAllCallback, AfterAllCallback,
    BeforeEachCallback, AfterEachCallback, ParameterResolver {

  private static final Logger LOGGER = LoggerFactory.getLogger(Extension.class);
  private static final String DOCKER_JUNIT_EXTENSION_LABEL = "docker-junit-extension";
  private static final String DOCKER_JUNIT_EXTENSION_VERSION =
      "docker-junit-extension-version";
  private static final String DOCKER_JUNIT_EXTENSION_RUNNER_ID_LABEL =
      "docker-junit-extension-runner-id";
  private static final File RUNNING_CONTAINERS_FILE = new File(
      System.getProperty("java.io.tmpdir") + "/"
          + Extension.class.getPackage().getName() + ".running.json");
  private static final boolean REMOVE_RUNNING_CONTAINERS = Boolean.getBoolean(
      Extension.class.getPackage().getName() + ".removeRunningContainers");
  private static final String DEFAULT_RUNNER_ID = "default";
  private static final String RUNNER_ID = System.getProperty(
      Extension.class.getPackage().getName() + ".runnerId", DEFAULT_RUNNER_ID);
  private static final boolean USE_RUNNING_CONTAINERS_FILE = Boolean.getBoolean(
      Extension.class.getPackage().getName() + ".useRunningContainersFile");
  private static final Version LAST_COMPATIBLE_INSTANCE_VERSION = new Version("1.2.12");
  private static final Version VERSION = Seq.of(Unchecked.supplier(
      () -> Extension.class.getResource("/docker-junit-extension.properties")).get())
      .map(Unchecked.function(url -> {
        Properties properties = new Properties();
        properties.load(url.openStream());
        return properties;
      }))
      .findFirst()
      .map(properties -> new Version(properties.getProperty("version")))
      .orElseThrow(() -> new RuntimeException(
          "Can not find version in /docker-junit-extension.properties resource"));
  private static final String DOCKER_REPOSITORY_CONFIG = System.getProperty(
      Extension.class.getPackage().getName() + ".dockerRepositoryConfig");
  private static final Optional<String> DOCKER_CLIENT = Optional.ofNullable(System.getProperty(
      Extension.class.getPackage().getName() + ".dockerClient"));

  private final ObjectMapper objectMapper;
  private final Map<String, String> containers = Collections.synchronizedMap(new HashMap<>());
  private final Map<Instance, List<String>> runningContainers =
      Collections.synchronizedMap(new HashMap<>());
  private DockerCompatibleClient dockerClient;

  {
    if (REMOVE_RUNNING_CONTAINERS) {
      Runtime.getRuntime().addShutdownHook(new Thread(Unchecked.runnable(() -> {
        LOGGER.info("Stop all running containers");
        Extension extension = new Extension();
        extension.loadRunningContainers();
        extension.stopRunningContainers();
      }), "docker-junit-extension-stop-running-containers"));
    }
  }

  Extension() throws Exception {
    this.objectMapper = new ObjectMapper();
    this.objectMapper.registerModule(new Jdk8Module());
  }

  @Override
  public void beforeAll(ExtensionContext context) throws Exception {
    Class<?> testClass = context.getTestClass().orElseThrow(NoSuchElementException::new);
    DockerExtension extensionAnnotation = testClass.getAnnotation(DockerExtension.class);
    DockerClientType dockerClientType = DOCKER_CLIENT
        .map(String::toUpperCase)
        .map(DockerClientType::valueOf)
        .orElse(extensionAnnotation.client());
    switch (dockerClientType) {
      case DOCKER_API:
        this.dockerClient = new DockerApiClient(DOCKER_REPOSITORY_CONFIG);
        break;
      case DOCKER:
        this.dockerClient = new DockerClient(DOCKER_REPOSITORY_CONFIG);
        break;
      case PODMAN:
        this.dockerClient = new PodmanClient(DOCKER_REPOSITORY_CONFIG);
        break;
      default:
        throw new RuntimeException("Unsupported type " + dockerClientType);
    }
    loadRunningContainers();
    ImmutableList<Tuple2<Instance, ? extends CompletableFuture<? extends Object>>> containers =
        Seq.of(extensionAnnotation.value())
        .filter(containerAnnotation -> containerAnnotation.whenReuse() != WhenReuse.EACH)
        .peek(containerAnnotation -> Preconditions.checkArgument(
            !this.containers.containsKey(containerAnnotation.alias()),
            "Alias '" + containerAnnotation.alias() + "' is already used"))
        .zipWithIndex()
        .map(t -> toInstance(context, t.v2, t.v1))
        .map(instance -> {
          if (instance.alias.isPresent()) {
            return Tuple.tuple(instance,
                CompletableFuture.runAsync(Blocking.runnable(Unchecked.runnable(() -> {
                  if (instance.stopIfChanged) {
                    new HashMap<>(runningContainers).entrySet().stream()
                        .filter(entry -> !entry.getKey().equals(instance))
                        .filter(entry -> entry.getKey().alias.equals(instance.alias))
                        .collect(Collectors.toList())
                        .stream()
                        .peek(entry -> runningContainers.remove(entry.getKey()))
                        .flatMap(entry -> entry.getValue().stream()
                            .map(sameAliasContainerId -> Tuple.tuple(
                                entry.getKey(), sameAliasContainerId)))
                        .forEach(Unchecked.consumer(t -> {
                          stopAndRemoveContainer(t.v1, t.v2);
                        }));
                  }
                  if (instance.whenReuse != WhenReuse.ALWAYS
                      || dockerClient.getContainerId(getAlias(instance.alias.get()))
                      .map(id -> new HashMap<>(runningContainers).values().stream()
                          .flatMap(runningContainerIds -> runningContainerIds.stream())
                          .noneMatch(runningContainerId -> id.equals(runningContainerId)))
                      .orElse(false)) {
                    stopAndRemoveContainerIfExists(instance);
                  }
                }))));
          }

          return Tuple.tuple(instance, CompletableFuture.completedFuture(null));
        })
        .collect(ImmutableList.toImmutableList());

    CountDownLatch countDownLatch = new CountDownLatch(containers.size());
    List<String> usedContainerIds = new ArrayList<>();

    Seq.seq(containers)
        .map(t -> {
          if (t.v1.whenReuse == WhenReuse.ALWAYS) {
            List<String> runningContainerIds = runningContainers
                .computeIfAbsent(t.v1, k -> new ArrayList<>());

            final Optional<String> runningContainerId;
            synchronized (usedContainerIds) {
              runningContainerId = runningContainerIds.stream()
                  .filter(containerId -> !usedContainerIds.contains(containerId))
                  .findFirst();

              if (runningContainerId.isPresent()) {
                usedContainerIds.add(runningContainerId.get());
              }
            }

            if (runningContainerId.isPresent()) {
              LOGGER.info("Reusing container for alias " + t.v1.configuredAlias);
              return Tuple.tuple(t.v1, t.v2
                  .thenAcceptAsync(Blocking.consumer(Unchecked.consumer(result -> {
                    if (t.v1.alias.isPresent()) {
                      Optional<String> sameAliasContainerId =
                          dockerClient.getContainerId(getAlias(t.v1.alias.get()));
                      if (sameAliasContainerId.isPresent()) {
                        dockerClient.renameContainer(
                            sameAliasContainerId.get(), Optional.empty());
                      }
                    }
                  })))
                  .thenAcceptAsync(result -> countDownLatch.countDown())
                  .thenAcceptAsync(Blocking.consumer(Unchecked.consumer(
                      result -> dockerClient.renameContainer(
                          runningContainerId.get(), getAlias(t.v1.alias)))))
                  .thenAcceptAsync(result -> this.containers.put(
                      t.v1.configuredAlias, runningContainerId.get())));
            }
          }

          return Tuple.tuple(t.v1, t.v2
              .thenAcceptAsync(result -> countDownLatch.countDown())
              .thenAcceptAsync(Blocking.consumer(Unchecked.consumer(result -> {
                if (t.v1.alias.isPresent()) {
                  Optional<String> containerId = dockerClient.getContainerId(
                      getAlias(t.v1.alias.get()));
                  if (containerId.map(id -> new HashMap<>(runningContainers).values().stream()
                      .flatMap(runningContainerIds -> runningContainerIds.stream())
                      .anyMatch(runningContainerId -> id.equals(runningContainerId)))
                      .orElse(false)) {
                    dockerClient.renameContainer(containerId.get(), Optional.empty());
                  } else {
                    stopAndRemoveContainerIfExists(t.v1);
                  }
                }
              })))
              .thenApplyAsync(Blocking.function(Unchecked.function(result -> startContainer(t.v1))))
              .thenAcceptAsync(containerId -> {
                if (t.v1.whenReuse == WhenReuse.ALWAYS) {
                  runningContainers.computeIfAbsent(t.v1, k -> new ArrayList<>()).add(containerId);
                }
                this.containers.put(t.v1.configuredAlias, containerId);
              }));
        })
        .map(t -> t.v2)
        .toList()
        .forEach(job -> job.join());
  }

  @Override
  public void beforeEach(ExtensionContext context) throws Exception {
    Class<?> testClass = context.getTestClass().orElseThrow(NoSuchElementException::new);
    DockerExtension extensionAnnotation = testClass.getAnnotation(DockerExtension.class);
    Seq.of(extensionAnnotation.value())
      .filter(containerAnnotation ->
          containerAnnotation.whenReuse() == WhenReuse.EACH)
      .peek(containerAnnotation -> Preconditions.checkArgument(
          !this.containers.containsKey(containerAnnotation.alias()),
          "Alias '" + containerAnnotation.alias() + "' is defined twice"))
      .zipWithIndex()
      .map(t -> toInstance(context, t.v2, t.v1))
      .map(instance -> CompletableFuture.supplyAsync(
            Blocking.supplier(Unchecked.supplier(() -> startContainer(instance))))
          .thenAcceptAsync(runningContainerId -> this.containers.put(
              instance.configuredAlias, runningContainerId)))
        .forEach(futureRunningContainer -> futureRunningContainer.join());
  }

  @Override
  public void afterAll(ExtensionContext context) throws Exception {
    storeRunningContainers();
    Class<?> testClass = context.getTestClass().orElseThrow(NoSuchElementException::new);
    DockerExtension extensionAnnotation = testClass.getAnnotation(DockerExtension.class);
    Seq.of(extensionAnnotation.value())
      .filter(containerAnnotation ->
        containerAnnotation.whenReuse() == WhenReuse.EACH_CLASS)
      .map(containerAnnotation -> toInstance(context, null, containerAnnotation))
      .map(instance -> CompletableFuture.supplyAsync(
              Blocking.supplier(Unchecked.supplier(() -> stopAndRemoveContainer(
                  instance))))
              .thenAcceptAsync(containerId -> this.containers.remove(containerId)))
      .toList()
        .forEach(job -> job.join());
  }

  @Override
  public void afterEach(ExtensionContext context) throws Exception {
    Class<?> testClass = context.getTestClass().orElseThrow(NoSuchElementException::new);
    DockerExtension extensionAnnotation = testClass.getAnnotation(DockerExtension.class);
    Seq.of(extensionAnnotation.value())
      .filter(containerAnnotation ->
        containerAnnotation.whenReuse() == WhenReuse.EACH)
      .map(containerAnnotation -> toInstance(context, null, containerAnnotation))
      .map(instance -> CompletableFuture.supplyAsync(
          Blocking.supplier(Unchecked.supplier(() -> stopAndRemoveContainer(instance))))
          .thenComposeAsync(containerId -> CompletableFuture.runAsync(
              () -> this.containers.remove(containerId))))
      .toList()
        .forEach(job -> job.join());
  }

  @Override
  public boolean supportsParameter(ParameterContext parameterContext,
      ExtensionContext extensionContext) throws ParameterResolutionException {
    return (parameterContext.getParameter().getType() == Container.class
            && parameterContext.isAnnotated(ContainerParam.class))
        || (parameterContext.getParameter().getType() == Docker.class
            && parameterContext.isAnnotated(DockerParam.class));
  }

  @Override
  public Object resolveParameter(ParameterContext parameterContext,
      ExtensionContext extensionContext) throws ParameterResolutionException {
    if (parameterContext.findAnnotation(ContainerParam.class).isPresent()) {
      return parameterContext.findAnnotation(ContainerParam.class)
          .map(containerParameter -> getContainer(containerParameter.value()))
          .orElseThrow(() -> new RuntimeException("Can not find "
              + ContainerParam.class.getName() + " annotation"));
    }

    if (parameterContext.findAnnotation(DockerParam.class).isPresent()) {
      return parameterContext.findAnnotation(DockerParam.class)
          .map(dockerParameter -> new Docker(this))
          .orElseThrow(() -> new RuntimeException("Can not find "
              + DockerParam.class.getName() + " annotation"));
    }

    throw new UnsupportedOperationException();
  }

  private void loadRunningContainers() throws Exception {
    if (!runningContainers.isEmpty()) {
      return;
    }

    if (USE_RUNNING_CONTAINERS_FILE) {
      if (!RUNNING_CONTAINERS_FILE.exists()) {
        return;
      }

      try {
        MapType runningContainersReference =
            objectMapper.getTypeFactory()
                .constructMapType(Map.class, String.class, Instance.class);
        Map<String, Instance> runningContainers =
            objectMapper.readValue(new String(Files.readAllBytes(RUNNING_CONTAINERS_FILE.toPath()),
                Charsets.UTF_8), runningContainersReference);
        for (Map.Entry<Instance, List<String>> entry : runningContainers
            .entrySet().stream().collect(Collectors.groupingBy(e -> e.getValue()))
            .entrySet().stream().collect(Collectors.toMap(
                e -> e.getKey(),
                e -> e.getValue().stream()
                  .map(v -> v.getKey())
                  .collect(Collectors.toList())))
            .entrySet()) {
          this.runningContainers.put(entry.getKey(), entry.getValue());
        }
      } catch (IOException ex) {
        LOGGER.error("Error while reading " + RUNNING_CONTAINERS_FILE, ex);
        throw ex;
      }
    } else {
      Map<String, String> versions = dockerClient.getContainerLabelValues(
          DOCKER_JUNIT_EXTENSION_VERSION);
      Map<String, String> runnerIds = dockerClient.getContainerLabelValues(
          DOCKER_JUNIT_EXTENSION_RUNNER_ID_LABEL);
      dockerClient.getContainerLabelValues(DOCKER_JUNIT_EXTENSION_LABEL)
          .entrySet()
          .stream()
          .filter(entry -> versions.containsKey(entry.getKey())
              && LAST_COMPATIBLE_INSTANCE_VERSION.compareTo(
                  new Version(versions.get(entry.getKey()))) <= 0)
          .filter(entry -> runnerIds.containsKey(entry.getKey())
              && RUNNER_ID.equals(runnerIds.get(entry.getKey())))
          .forEach(entry -> {
            try {
              Instance instance = objectMapper.readValue(entry.getValue(), Instance.class);
              this.runningContainers.computeIfAbsent(instance, i -> new ArrayList<>())
                  .add(entry.getKey());
            } catch (Exception ex) {
              LOGGER.error("Error while parsing label"
                  + " " + DOCKER_JUNIT_EXTENSION_LABEL
                  + " for container " + entry.getKey(), ex);
            }
          });
    }

    runningContainers.entrySet()
        .stream()
        .collect(Collectors.toList())
        .stream()
        .flatMap(entry -> entry.getValue().stream()
            .map(v -> Tuple.tuple(entry.getKey(), v)))
        .forEach(t -> {
          try {
            if (!dockerClient.isContainerRunning(t.v2)) {
              LOGGER.warn("Stored container not running");
              runningContainers.remove(t.v1);
              t.v1.mounts.stream().filter(mount -> !mount.system)
                  .forEach(mount -> mount.deleteTemp());
            } else {
              if (t.v1.mounts.stream().filter(mount -> !mount.isSystem())
                  .anyMatch(mount -> !mount.existsTemp())) {
                LOGGER.warn("Stored container has some missing temp");
                runningContainers.remove(t.v1);
                stopAndRemoveContainer(t.v1);
              }
            }
          } catch (NotFoundException ex) {
            LOGGER.warn("Stored container not found");
            runningContainers.remove(t.v1);
          } catch (Exception ex) {
            throw new RuntimeException(ex);
          }
        });
  }

  private void storeRunningContainers() throws Exception {
    if (!USE_RUNNING_CONTAINERS_FILE) {
      return;
    }

    MapType runningContainersReference =
        objectMapper.getTypeFactory()
            .constructMapType(Map.class, String.class, Instance.class);
    try {
      if (RUNNING_CONTAINERS_FILE.exists()) {
        RUNNING_CONTAINERS_FILE.delete();
      }
      Map<String, Instance> runningContainers = Maps.newHashMap();
      for (Map.Entry<Instance, String> entry : this.runningContainers
          .entrySet().stream()
          .flatMap(entry -> entry.getValue().stream()
              .map(i -> Tuple.tuple(entry.getKey(), i)))
          .collect(Collectors.toMap(t -> t.v1, t -> t.v2)).entrySet()) {
        if (runningContainers.containsKey(entry.getValue())) {
          throw new RuntimeException("Can not save state");
        }
        runningContainers.put(entry.getValue(), entry.getKey());
      }
      String json = objectMapper
          .writerFor(runningContainersReference)
          .writeValueAsString(runningContainers);
      Files.write(RUNNING_CONTAINERS_FILE.toPath(),
          json.getBytes(Charsets.UTF_8),
          (OpenOption) StandardOpenOption.CREATE,
          (OpenOption) StandardOpenOption.WRITE,
          (OpenOption) StandardOpenOption.TRUNCATE_EXISTING);
    } catch (IOException ex) {
      LOGGER.error("Error storing currently used containers", ex);
      if (RUNNING_CONTAINERS_FILE.exists()) {
        RUNNING_CONTAINERS_FILE.delete();
      }
    }
  }

  private void stopRunningContainers() {
    this.runningContainers
        .entrySet()
        .stream()
        .flatMap(e -> e.getValue().stream()
            .map(containerId -> Tuple.tuple(e.getKey(), containerId)))
        .forEach(t -> {
          try {
            stopAndRemoveContainer(t.v1, t.v2);
          } catch (Exception ex) {
            LOGGER.warn(
                "Unable to remove container " + t.v1.configuredAlias + " (" + t.v2 + ")", ex);
          }
        });
  }

  Container getContainer(String alias) {
    return new Container(
        dockerClient, getContainerIdFromAlias(alias));
  }

  DockerCompatibleClient getDockerClient() {
    return dockerClient;
  }

  String getAlias(String alias) {
    if (RUNNER_ID.equals(DEFAULT_RUNNER_ID)) {
      return alias;
    }
    return alias + "-" + RUNNER_ID;
  }

  Optional<String> getAlias(Optional<String> alias) {
    return alias.map(a -> getAlias(a));
  }

  private String startContainer(Instance instance) throws Exception {
    String image = instance.image;
    Optional<String> alias = instance.alias;
    List<String> arguments = instance.arguments;
    Map<String, String> environment = instance.environment;
    Set<PortBinding> ports = instance.ports;
    List<MountBinding> mounts = instance.mounts;

    int count = instance.retry;

    mounts.stream().filter(mount -> !mount.system).forEach(mount -> mount.copyToTemp());

    while (count-- > 0) {
      LOGGER.info("Starting container for alias " + instance.configuredAlias);
      String containerId = dockerClient.startContainer(getAlias(alias), image,
          arguments, environment, ports, mounts, ImmutableMap.of(
              DOCKER_JUNIT_EXTENSION_LABEL, objectMapper
                  .writerFor(Instance.class)
                  .writeValueAsString(instance),
              DOCKER_JUNIT_EXTENSION_RUNNER_ID_LABEL, RUNNER_ID,
              DOCKER_JUNIT_EXTENSION_VERSION, VERSION.get()));
      if (waitForLog(containerId, instance.expectedLogs,
            instance.expectedLogTimeout)) {
        return containerId;
      }

      dockerClient.stopAndRemoveContainer(containerId);
    }

    throw new IllegalStateException("Container can not be created after "
        + instance.retry + " retries");
  }

  private boolean waitForLog(String id, String[] expectedLogs, int timeout)
      throws InterruptedException, DockerException, ExecutionException, IOException {
    return Arrays.equals(WaitFor.NOTHING, expectedLogs)
        || dockerClient.waitForLog(expectedLogs, id, Duration.ofMillis(timeout));
  }

  private String stopAndRemoveContainer(Instance instance)
      throws Exception {
    String containerId = dockerClient.getContainerId(getAlias(instance.configuredAlias))
        .orElseThrow(() -> new NoSuchElementException());
    return stopAndRemoveContainer(instance, containerId);
  }

  protected String stopAndRemoveContainer(Instance instance, String containerId)
      throws DockerException, InterruptedException, IOException {
    LOGGER.info("Stopping container {}", instance.configuredAlias);
    dockerClient.stopAndRemoveContainer(containerId);
    instance.mounts.stream().filter(mount -> !mount.system).forEach(mount -> mount.deleteTemp());
    return containerId;
  }

  private void stopAndRemoveContainerIfExists(Instance instance) throws Exception {
    LOGGER.info("Stopping container if exists {}", instance.configuredAlias);
    dockerClient.stopAndRemoveContainerIfExists(getAlias(instance.configuredAlias));
    instance.mounts.stream().filter(mount -> !mount.system).forEach(mount -> mount.deleteTemp());
  }

  private Instance toInstance(ExtensionContext context, Long index,
      DockerContainer containerAnnotation) {
    return Instance.fromAnnotation(context, index, containerAnnotation);
  }

  private String getContainerIdFromAlias(String alias) {
    if (!this.containers.containsKey(alias)) {
      throw new RuntimeException("Alias '" + alias + "' has no associated container");
    }
    return this.containers.get(alias);
  }
}
