/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import com.github.dockerjava.api.exception.DockerException;
import com.google.common.collect.ImmutableMap;

public class Docker {

  private final Extension extension;

  protected Docker(Extension extension) {
    super();
    this.extension = extension;
  }

  /**
   * Get a running container.
   */
  public Container getContainer(String alias) throws DockerException, InterruptedException {
    return extension.getContainer(alias);
  }

  /**
   * Run a container command and return its output.
   */
  public Stream<String> run(Instance instance, String...args)
      throws DockerException, InterruptedException, IOException {
    String image = instance.image;
    Optional<String> alias = instance.alias;
    List<String> arguments = instance.arguments;
    Map<String, String> environment = instance.environment;
    Set<PortBinding> ports = instance.ports;
    List<MountBinding> mounts = instance.mounts;

    return extension.getDockerClient().runContainer(extension.getAlias(alias), image,
        args.length > 0 ? Arrays.asList(args) : arguments, environment, ports, mounts,
            ImmutableMap.of());
  }

}
