/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 - 2020 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import com.github.dockerjava.api.exception.DockerException;

public interface DockerCompatibleClient {

  String startContainer(Optional<String> name, String image, List<String> arguments,
      Map<String, String> environment, Collection<PortBinding> ports,
      Collection<MountBinding> mounts, Map<String, String> labels)
      throws DockerException, InterruptedException, IOException;

  void renameContainer(String id, Optional<String> alias)
      throws DockerException, InterruptedException, IOException;

  Optional<String> getContainerId(String alias)
      throws DockerException, InterruptedException, IOException;

  Map<String, String> getContainerLabelValues(String label)
      throws DockerException, InterruptedException, IOException;

  String getContainerAlias(String id) throws DockerException, InterruptedException, IOException;

  boolean isContainerRunning(String id) throws DockerException, InterruptedException, IOException;

  void stopAndRemoveContainerIfExists(String alias)
      throws DockerException, InterruptedException, IOException;

  void stopAndRemoveContainer(String containerId)
      throws DockerException, InterruptedException, IOException;

  boolean waitForLog(String[] expectedLines, String containerId, Duration timeout)
      throws InterruptedException, DockerException, ExecutionException, IOException;

  String getContainerIp(String containerId)
      throws DockerException, InterruptedException, IOException;

  Integer getContainerBindedPort(String containerId, Integer port, NetworkProtocol protocol)
      throws DockerException, InterruptedException, IOException;

  Stream<String> execute(String containerId, String... args)
      throws DockerException, InterruptedException, IOException;

  Stream<String> runContainer(Optional<String> alias, String image, List<String> arguments,
      Map<String, String> environment, Set<PortBinding> ports, Collection<MountBinding> mounts,
      Map<String, String> labels) throws DockerException, InterruptedException, IOException;

  void copyToContainer(String containerId, Path path, String internalPath)
      throws DockerException, InterruptedException, IOException;

  void copyToContainer(String containerId, InputStream inputStream, String internalPath)
      throws DockerException, InterruptedException, IOException;

  InputStream copyFromContainer(String containerId, String internalPath)
      throws DockerException, InterruptedException, IOException;

}
