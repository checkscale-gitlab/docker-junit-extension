/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.io.Closeable;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.github.dockerjava.api.async.ResultCallbackTemplate;
import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.command.ExecCreateCmdResponse;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.command.InspectExecResponse;
import com.github.dockerjava.api.command.PullImageResultCallback;
import com.github.dockerjava.api.exception.DockerException;
import com.github.dockerjava.api.exception.NotModifiedException;
import com.github.dockerjava.api.model.Bind;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Frame;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.Identifier;
import com.github.dockerjava.api.model.InternetProtocol;
import com.github.dockerjava.api.model.Ports.Binding;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient;
import com.github.dockerjava.transport.DockerHttpClient;
import com.google.common.base.Charsets;
import org.jooq.lambda.Blocking;
import org.jooq.lambda.Seq;
import org.jooq.lambda.Unchecked;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class DockerApiClient extends AbstractDockerCompatibleClient implements DockerCompatibleClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(DockerApiClient.class);
  private static final int SECONDS_TO_WAIT_BEFORE_KILLING = 3;

  public DockerApiClient(String imageRegistryConfigsPath) {
    super(imageRegistryConfigsPath);
  }

  @Override
  public String startContainer(Optional<String> name, String image, List<String> arguments,
      Map<String, String> environment, Collection<PortBinding> ports,
      Collection<MountBinding> mounts, Map<String, String> labels)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClientForImage(image)) {
      String containerId = createContainer(client,
          name, image, arguments, environment, ports, mounts, labels);
      client.startContainerCmd(containerId)
          .exec();
      return containerId;
    }
  }

  private String createContainer(com.github.dockerjava.api.DockerClient client,
      Optional<String> name, String image, List<String> arguments,
      Map<String, String> environment, Collection<PortBinding> ports,
      Collection<MountBinding> mounts, Map<String, String> labels)
      throws DockerException, InterruptedException, IOException {
    final String imageToPull = getImageToPull(image);
    if (client.listImagesCmd().exec().stream()
        .noneMatch(
            foundImage -> foundImage.getRepoTags() != null
                ? Arrays.asList(foundImage.getRepoTags()).contains(image)
                : false)) {
      PullImageResultCallback callback = new PullImageResultCallback();
      client.pullImageCmd(imageToPull).exec(callback);
      callback.awaitCompletion();
      if (!imageToPull.equals(image)) {
        final Identifier identifier = Identifier.fromCompoundString(image);
        client.tagImageCmd(imageToPull, identifier.repository.name,
            identifier.tag.orElse("latest"))
            .exec();
      }
    }
    CreateContainerCmd createContainer = client.createContainerCmd(image);
    if (name.isPresent()) {
      createContainer.withName(name.get());
    }
    String containerId = createContainer
        .withLabels(labels)
        .withCmd(arguments)
        .withEnv(createEnvironmentList(environment))
        .withHostConfig(HostConfig.newHostConfig()
            .withPortBindings(ports.stream()
                .map(port -> new com.github.dockerjava.api.model.PortBinding(
                    new Binding("localhost", port.getExternalPort()
                        .map(String::valueOf).orElse(null)),
                    new ExposedPort(port.getInternalPort(),
                        InternetProtocol.valueOf(port.getNetworkProtocol().name()))))
                .collect(Collectors.toList()))
            .withBinds(mounts.stream()
                .peek(mount -> {
                  if (!Files.exists(Paths.get(mount.getSystemPath()))) {
                    LOGGER.warn("Skipping mount of {} since does not exists",
                        mount.getDockerIdentifier());
                  }
                })
                .filter(mount -> Files.exists(Paths.get(mount.getSystemPath())))
                .map(mount -> mount.getDockerIdentifier())
                .map(Bind::parse)
                .collect(Collectors.toList())))
        .exec().getId();
    return containerId;
  }

  @Override
  public void renameContainer(String id, Optional<String> alias)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      if (alias.isPresent()) {
        if (!client.inspectContainerCmd(id).exec().getName().substring(1).equals(alias.get())) {
          client.renameContainerCmd(id)
            .withName(alias.get())
              .exec();
        }
      } else {
        if (client.listImagesCmd().exec().stream()
            .noneMatch(foundImage -> foundImage.getRepoTags() != null
                ? Arrays.asList(foundImage.getRepoTags()).contains(HELLO_WORLD_IMAGE)
                : false)) {
          PullImageResultCallback callback = new PullImageResultCallback();
          client.pullImageCmd(HELLO_WORLD_IMAGE)
              .exec(callback);
          callback.awaitCompletion();
        }
        String tmpId = client.createContainerCmd(HELLO_WORLD_IMAGE)
            .withHostConfig(HostConfig.newHostConfig())
            .exec()
            .getId();
        String tmpAlias;
        try {
          tmpAlias = client.inspectContainerCmd(tmpId).exec().getName().substring(1);
        } finally {
          client.removeContainerCmd(tmpId)
            .withRemoveVolumes(true)
              .exec();
        }
        client.renameContainerCmd(id)
            .withName(tmpAlias);
      }
    }
  }

  @Override
  public Optional<String> getContainerId(String alias)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      return client.listContainersCmd()
          .withShowAll(true)
          .exec().stream()
          .filter(container -> Arrays.asList(
              container.getNames()).contains('/' + alias)).findFirst()
          .map(container -> container.getId());
    }
  }

  @Override
  public Map<String, String> getContainerLabelValues(String label)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      return client.listContainersCmd()
          .withShowAll(true)
          .exec().stream()
          .filter(container -> container.getLabels().containsKey(label))
          .collect(Collectors.toMap(
              container -> container.getId(),
              container -> container.getLabels().get(label)));
    }
  }

  @Override
  public String getContainerAlias(String id)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      return client.inspectContainerCmd(id).exec().getName().substring(1);
    }
  }

  @Override
  public boolean isContainerRunning(String id)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      return client.inspectContainerCmd(id).exec().getState().getRunning();
    }
  }

  @Override
  public void stopAndRemoveContainerIfExists(String alias)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      client.listContainersCmd()
          .withShowAll(true)
          .exec().stream()
          .filter(container -> Arrays.asList(
              container.getNames()).contains('/' + alias)).findFirst()
          .ifPresent(container -> {
            if (container.getState().equals("running")) {
              client.stopContainerCmd(container.getId())
                .withTimeout(SECONDS_TO_WAIT_BEFORE_KILLING)
                  .exec();
            }
            client.removeContainerCmd(container.getId())
              .withRemoveVolumes(true)
                .exec();
          });
    }
  }

  @Override
  public void stopAndRemoveContainer(String containerId)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      stopContainer(client, containerId);
      client.removeContainerCmd(containerId)
        .withRemoveVolumes(true)
          .exec();
    }
  }

  private void stopContainer(com.github.dockerjava.api.DockerClient client, String containerId) {
    try {
      client.stopContainerCmd(containerId)
        .withTimeout(SECONDS_TO_WAIT_BEFORE_KILLING)
          .exec();
    } catch (NotModifiedException ex) {
      return;
    }
  }

  @Override
  public boolean waitForLog(String[] expectedLines, String containerId, Duration timeout)
      throws InterruptedException, DockerException, ExecutionException, IOException {
    try {
      com.github.dockerjava.api.DockerClient client = createDockerClient();
      String containerAlias = getContainerAlias(containerId);
      FrameIterator frameIterator = new FrameIterator();
      client.attachContainerCmd(containerId)
        .withLogs(true)
        .withStdOut(true)
        .withStdErr(true)
        .withFollowStream(true)
          .exec(frameIterator);
      LogStreamIterator logStreamIterator = new LogStreamIterator(
          client, containerId,
          frameIterator, false);
      List<String> expectedLineList = Seq.of(expectedLines).toList();
      return CompletableFuture.supplyAsync(Blocking.supplier(Unchecked.supplier(
          () -> Seq.seq(logStreamIterator)
            .peek(line -> {
              LOGGER.trace("[{}:{}] {}",
                  containerAlias, containerId.substring(0, 10), line);
              if (line.contains(expectedLineList.get(0))) {
                expectedLineList.remove(0);
              }
            })
            .filter(line -> expectedLineList.isEmpty())
            .findAny().isPresent())))
          .thenApply(result -> {
            Unchecked.runnable(logStreamIterator::close).run();
            return result;
          })
          .get(timeout.toMillis(), TimeUnit.MILLISECONDS);
    } catch (TimeoutException ex) {
      return false;
    }
  }

  @Override
  public String getContainerIp(String containerId)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      return client.inspectContainerCmd(containerId).exec().getNetworkSettings().getNetworks()
          .entrySet().stream().findAny().get().getValue().getIpAddress();
    }
  }

  @Override
  public Integer getContainerBindedPort(String containerId, Integer port, NetworkProtocol protocol)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      return Integer.valueOf(
          client.inspectContainerCmd(containerId).exec().getNetworkSettings().getPorts()
          .getBindings().entrySet().stream()
          .filter(e -> protocol.name().equals(e.getKey().getProtocol().name())
              && port.equals(e.getKey().getPort()))
          .flatMap(e -> Stream.of(e.getValue()))
          .map(b -> b.getHostPortSpec())
          .findFirst()
          .get());
    }
  }

  @Override
  public Stream<String> execute(String containerId, String... args)
      throws DockerException, InterruptedException, IOException {
    com.github.dockerjava.api.DockerClient client = createDockerClient();
    ExecCreateCmdResponse execCreate =
        client.execCreateCmd(containerId)
          .withCmd(args)
          .withAttachStdin(false)
          .withAttachStdout(true)
          .withAttachStderr(true)
          .exec();
    FrameIterator frameIterator = new FrameIterator();
    client.execStartCmd(execCreate.getId())
        .exec(frameIterator);

    LogStreamIterator logStreamIterator = new LogStreamIterator(
        execCreate,
        args,
        client,
        containerId,
        frameIterator);
    return Seq.seq(logStreamIterator)
        .onClose(Unchecked.runnable(logStreamIterator::close));
  }

  @Override
  public Stream<String> runContainer(Optional<String> alias, String image, List<String> arguments,
      Map<String, String> environment, Set<PortBinding> ports, Collection<MountBinding> mounts,
      Map<String, String> labels) throws DockerException, InterruptedException, IOException {
    com.github.dockerjava.api.DockerClient client = createDockerClientForImage(image);
    String containerId = createContainer(client,
        alias, image, arguments, environment, ports, mounts, labels);
    client.startContainerCmd(containerId).exec();
    FrameIterator frameIterator = new FrameIterator();
    client.attachContainerCmd(containerId)
      .withLogs(true)
      .withStdOut(true)
      .withStdErr(true)
      .withFollowStream(true)
        .exec(frameIterator);
    LogStreamIterator logStreamIterator = new LogStreamIterator(client, containerId,
        frameIterator, true);
    return Seq.seq(logStreamIterator).onClose(Unchecked.runnable(logStreamIterator::close));
  }

  @Override
  public void copyToContainer(String containerId, Path path, String internalPath)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      client.copyArchiveToContainerCmd(containerId)
        .withHostResource(path.toAbsolutePath().toString())
        .withRemotePath(internalPath)
          .exec();
    }
  }

  @Override
  public void copyToContainer(String containerId, InputStream inputStream, String internalPath)
      throws DockerException, InterruptedException, IOException {
    try (com.github.dockerjava.api.DockerClient client = createDockerClient()) {
      client.copyArchiveToContainerCmd(containerId)
        .withTarInputStream(inputStream)
        .withRemotePath(internalPath)
          .exec();
    }
  }

  @Override
  public InputStream copyFromContainer(String containerId, String internalPath)
      throws DockerException, InterruptedException, IOException {
    com.github.dockerjava.api.DockerClient client = createDockerClient();
    return new DockerClientInputStream(client, client.copyArchiveFromContainerCmd(
        containerId, internalPath).exec());
  }

  private com.github.dockerjava.api.DockerClient createDockerClientForImage(String image) {
    final ImageRepositoryConfig imageRepositoryConfig = getImageRepositoryConfig(image);
    if (imageRepositoryConfig == null) {
      return createDockerClient();
    }
    DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
        .withRegistryUsername(imageRepositoryConfig.getUsername())
        .withRegistryPassword(imageRepositoryConfig.getPassword())
        .withRegistryUrl(imageRepositoryConfig.getUrl()).build();
    DockerHttpClient httpClient = new ApacheDockerHttpClient.Builder()
        .dockerHost(config.getDockerHost())
        .sslConfig(config.getSSLConfig())
        .build();

    return DockerClientImpl.getInstance(config, httpClient);
  }

  private com.github.dockerjava.api.DockerClient createDockerClient() {
    DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder().build();
    DockerHttpClient httpClient = new ApacheDockerHttpClient.Builder()
        .dockerHost(config.getDockerHost())
        .sslConfig(config.getSSLConfig())
        .build();

    return DockerClientImpl.getInstance(config, httpClient);
  }

  private class DockerClientInputStream extends FilterInputStream {
    private final com.github.dockerjava.api.DockerClient client;

    public DockerClientInputStream(com.github.dockerjava.api.DockerClient client,
        InputStream inputStream) {
      super(inputStream);
      this.client = client;
    }

    @Override
    public void close() throws IOException {
      super.close();
      client.close();
    }
  }

  private class LogStreamIterator implements Iterator<String>, Closeable {

    private final Scanner scanner;
    private String line = null;

    private LogStreamIterator(ExecCreateCmdResponse execCreate, String[] args,
        com.github.dockerjava.api.DockerClient client,
        String containerId, FrameIterator frameIterator) {
      this.scanner = new Scanner(new InputStreamReader(
          new ExecLogInputStream(
              execCreate, args, client, containerId, frameIterator), Charsets.UTF_8));
    }

    private LogStreamIterator(com.github.dockerjava.api.DockerClient client,
        String containerId,
        FrameIterator frameIterator, boolean waitForContainer) {
      this.scanner = new Scanner(new InputStreamReader(
          new LogInputStream(client, containerId, frameIterator, waitForContainer),
          Charsets.UTF_8));
    }

    @Override
    public boolean hasNext() {
      if (line != null) {
        return true;
      }

      while (line == null) {
        if (!scanner.hasNext()) {
          try {
            this.scanner.close();
          } catch (RuntimeException ex) {
            throw ex;
          } catch (Exception ex) {
            throw new RuntimeException(ex);
          }
          return false;
        } else {
          line = stripEmptyBuffer(scanner.nextLine());
        }
      }

      return true;
    }

    @Override
    public String next() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }

      try {
        return line;
      } finally {
        line = null;
      }
    }

    @Override
    public void close() throws IOException {
      this.scanner.close();
    }

  }

  private class LogInputStream extends InputStream {
    private final com.github.dockerjava.api.DockerClient client;
    private final String containerId;
    private final FrameIterator frameIterator;
    private final boolean waitForContainer;
    private byte[] byteBuffer = null;
    private int byteBufferIndex = 0;

    public LogInputStream(com.github.dockerjava.api.DockerClient client,
        String containerId, FrameIterator frameIterator, boolean waitForContainer) {
      this.client = client;
      this.containerId = containerId;
      this.frameIterator = frameIterator;
      this.waitForContainer = waitForContainer;
    }

    @Override
    public int read() throws IOException {
      try {
        if (byteBuffer == null || byteBufferIndex >= byteBuffer.length) {
          if (frameIterator.hasNext()) {
            byteBuffer = frameIterator.next();
            byteBufferIndex = 0;
          } else {
            return -1;
          }
        }

        int read = byteBuffer[byteBufferIndex++] & 0xFF;

        return read;
      } catch (IllegalStateException ex) {
        return -1;
      } catch (RuntimeException ex) {
        if (ex.getCause() instanceof IOException) {
          return -1;
        }
        throw ex;
      }
    }

    @Override
    public void close() throws IOException {
      frameIterator.close();

      if (waitForContainer) {
        waitForContainer();
      }

      closeDockerClient();
    }

    protected void closeDockerClient() throws IOException {
      client.close();
    }

    protected void waitForContainer() throws IOException {
      InspectContainerResponse inspectContainerResponse;
      while (true) {
        inspectContainerResponse = client.inspectContainerCmd(containerId).exec();
        if (!inspectContainerResponse.getState().getRunning()) {
          break;
        }
        Unchecked.runnable(() -> TimeUnit.MILLISECONDS.sleep(100)).run();
      }

      long exitCode = inspectContainerResponse.getState().getExitCodeLong();

      Unchecked.runnable(() -> stopAndRemoveContainer(containerId)).run();

      if (exitCode != 0) {
        throw new RuntimeException("Command "
            + Seq.of(inspectContainerResponse.getArgs()).toString(" ")
            + " exited with code " + inspectContainerResponse.getState().getExitCodeLong());
      }
    }
  }

  private class ExecLogInputStream extends LogInputStream {
    private final com.github.dockerjava.api.DockerClient client;
    private final ExecCreateCmdResponse execCreate;
    private final String[] args;

    public ExecLogInputStream(ExecCreateCmdResponse execCreate, String[] args,
        com.github.dockerjava.api.DockerClient client,
        String containerId, FrameIterator frameIterator) {
      super(client, containerId, frameIterator, false);
      this.client = client;
      this.execCreate = execCreate;
      this.args = args;
    }

    @Override
    protected void closeDockerClient() throws IOException {
    }

    @Override
    public void close() throws IOException {
      super.close();

      Optional<InspectExecResponse> execState;
      while (true) {
        execState = Optional.ofNullable(
            client.inspectExecCmd(execCreate.getId()).exec());
        if (execState.map(state -> state.getExitCodeLong() != null).orElse(true)) {
          break;
        }
        Unchecked.runnable(() -> TimeUnit.MILLISECONDS.sleep(100)).run();
      }

      if (execState.map(state -> state.getExitCodeLong() != 0).orElse(false)) {
        throw new RuntimeException("Command " + Seq.of(args).toString(" ")
            + " exited with code " + execState
              .map(state -> state.getExitCodeLong().toString()).orElse("unknown"));
      }

      client.close();
    }
  }

  private static final String EMPTY_BUFFER_AS_STRING = Seq.range(0, 256)
      .map(i -> "-585138796028742679682637458412L")
      .toString("");
  private static final byte[] EMPTY_BUFFER = toBytes(EMPTY_BUFFER_AS_STRING);

  private static byte[] toBytes(String string) {
    byte[] bytes = new byte[string.length()];
    for (int index = 0; index < string.length(); index++) {
      bytes[index] = (byte) string.charAt(index);
    }
    return bytes;
  }

  private static String stripEmptyBuffer(String line) {
    int emptyBufferStart = line.indexOf(EMPTY_BUFFER_AS_STRING);
    if (emptyBufferStart >= 0) {
      if (line.length() == EMPTY_BUFFER_AS_STRING.length()) {
        return null;
      }
      return line.replace(EMPTY_BUFFER_AS_STRING, "");
    }
    return line;
  }

  private class FrameIterator
      extends ResultCallbackTemplate<FrameIterator, Frame>
      implements Iterator<byte[]>, Closeable {

    private final ArrayBlockingQueue<byte[]> frames = new ArrayBlockingQueue<>(1);
    private byte[] frame = null;
    private byte[] previousFrame = null;
    private Throwable throwable = null;
    private boolean completed = false;

    @Override
    public boolean hasNext() {
      Instant end = Instant.now().plusMillis(10);
      while (frame == null) {
        frame = frames.poll();
        if (frame == null && completed) {
          if (throwable != null) {
            if (throwable instanceof RuntimeException) {
              throw (RuntimeException) throwable;
            }
            throw new RuntimeException(throwable);
          }
          return false;
        }

        if (frame == null) {
          try {
            if (previousFrame != null
                && previousFrame != EMPTY_BUFFER
                && Instant.now().isAfter(end)) {
              frame = EMPTY_BUFFER;
            }
            Thread.sleep(1);
          } catch (InterruptedException ex) {
            completed = true;
            throw new RuntimeException(ex);
          }
        }
      }

      return true;
    }

    @Override
    public byte[] next() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }

      try {
        return frame;
      } finally {
        previousFrame = frame;
        frame = null;
      }
    }

    @Override
    public void onNext(Frame item) {
      try {
        this.frames.put(item.getPayload());
      } catch (InterruptedException ex) {
        onError(ex);
      }
    }

    @Override
    public void onError(Throwable throwable) {
      completed = true;
      this.throwable = throwable;
    }

    @Override
    public void onComplete() {
      this.completed = true;
    }
  }
}
