/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.util.regex.Pattern;

import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;

@DockerExtension(value = {
    @DockerContainer(
        alias = "postgres",
        extendedBy = DockerExtensionConfiguration.class,
        whenReuse = WhenReuse.ALWAYS,
        stopIfChanged = true)
    }, client = DockerClientType.DOCKER)
@DisabledIfEnvironmentVariable(named = "DOCKER_EXTENSION_TESTS",
    matches = "^((?!(?:^| )DOCKER(?:$| )).)*$")
public class DockerExtensionDockerIt extends AbstractDockerExtensionIt {

  @Override
  protected String execFailTestExceptionMessagePattern() {
    return Pattern.quote("Command docker ")
        + "((exec|run) [^ ]+ "
        + Pattern.quote("sh -c echo 1; false") + "|start -a [^ ]+)"
        + Pattern.quote(" exited with code 1");
  }
}
